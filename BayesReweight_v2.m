function BayesReweight_v2(T,P,dim_UMB,periodicity_UMB,TPFile_UMB,harmonicBiasesFile_UMB,trajDir_UMB,histBinEdgesFile_UMB,histDir_UMB,fMAPFile_UMB,fMHFile_UMB,trajDir_PROJ,histBinEdgesFile_PROJ)

% Copyright:	Andrew L. Ferguson, UIUC 
% Last updated:	1 Jun 2017

% SYNOPSIS
%
% code to perform reweighting of dim_UMB-dimensional free energy surface inferred from Bayesian inference of biased umbrella sampling simulations in dim_UMB-dimensional variables, psi, into arbitrary dim_TRAJ-dimensional auxiliary variables, xi, recorded during umbrella sampling runs    
%
% 1. assumes 	(i)   harmonic restraining potentials in dim_UMB-dimensional umbrella variables psi 
%            	(ii)  NPT simulation data 
%            	(iii) rectilinear binning of histograms collected over each umbrella simulation  
% 2. requires 	(i)   trajectory of dim_UMB-dimensional umbrella variables, psi, recorded over each biased simulation, and biased histograms compiled over each biased run 
%				(ii)  maximum a posteriori (MAP) estimates of f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i, for i=1..S biased simulations 
%				(iii) Metropolis-Hastings (MH) samples from the Bayes posterior of f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i, for i=1..S biased simulations 
%				(iii) trajectory of dim_TRAJ-dimensional projection variables, xi, collected over each biased simulation at same time steps as the recorded umbrella variables 

% INPUTS
%
% T                         - [float] temperature in Kelvin at which to construct the projected landscape 
% P                         - [float] pressure in bar at which to construct the projected landscape 
% dim_UMB					- [int] dimensionality of umbrella sampling data in psi(1:dim_UMB) = number of coordinates in which umbrella sampling in psi was conducted 
% periodicity_UMB			- [1 x dim_UMB bool] periodicity in each dimension across range of histogram bins 
%                             -> periodicity(i) == 0 => no periodicity in dimension i; periodicity(i) == 1 => periodicity in dimension i with period defined by range of histogram bins 
% TPFile_UMB                - [str] path to text file containing 3 columns and S rows listing temperature in Kelvin and pressure in bar at which each simulation was conducted 
%                             -> col 1 = umbrella simulation index 1..S, col 2 = temperature / K, col 3 = pressure / bar 
% harmonicBiasesFile_UMB	- [str] path to text file containing (1 + 2*dim_UMB) columns and S rows listing location and strength of biasing potentials in each of the S umbrella simulations 
%                             -> col 1 = umbrella simulation index 1..S, col 2:(2+dim_UMB-1) = biased simulation umbrella centers / (arbitrary units), col (2+dim_UMB):(2+2*dim_UMB-1) = harmonic restraining potential force constant / kJ/mol.(arbitrary units)^2 
% trajDir_UMB				- [str] path to directory holding i=1..S Dim_UMB=(2+dim_UMB)-dimensional trajectories in files traj_i.txt recording trajectory of U, V, and dim_UMB-dimensional umbrella variables psi over each biased simulation 
%							  -> each file contains N_i rows constituting the number of samples recorded over the run, each containing Dim_UMB=(2+dim_UMB) columns recording the value of U, V, and the umbrella variables psi(1:dim_UMB) 
%							  -> the file trajDir_UMB/traj_i.txt constitutes the raw data from which the histogram in histDir_UMB/hist_i.txt was constructed 
% histBinEdgesFile_UMB		- [str] path to text file containing Dim_UMB=(2+dim_UMB) rows specifying edges of the rectilinear histogram bins in each dimension used to construct Dim_UMB=(2+dim_UMB)-dimensional histograms held in histDir/hist_i.txt 
%							  -> histBinEdgesFile_UMB contains k=1..Dim_UMB lines each holding a row vector specifying the rectilinear histogram bin edges in dimension k 
%                             -> for M_k bins in dimension k, there are (M_k+1) edges 
%                             -> bins need not be regularly spaced 
% histDir_UMB				- [str] path to directory holding i=1..S Dim_UMB=(2+dim_UMB)-dimensional histograms in files hist_i.txt compiled from S biased trajectories over Dim_UMB-dimensional rectilinear histogram grid specified in histBinEdgesFile  
%                             -> hist_i.txt comprises a row vector containing product_k=1..Dim_UMB M_k = (M_1*M_2*...*M_Dim_UMB) values recording histogram counts in each bin of the rectilinear histogram 
%                             -> values recorded in row major order (last index changes fastest) 
%							  -> the file trajDir_UMB/traj_i.txt constitutes the raw data from which the histogram in histDir_UMB/hist_i.txt was constructed 
% fMAPFile_UMB				- [str] path to file containing MAP estimates of f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i, for i=1..S biased simulations 
%							  -> S values stored as a row vector 
% fMHFile_UMB				- [str] path to file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of f_i = Z/Z_i = ratio of unbiased partition function to that of biased simulation i, for i=1..S biased simulations 
%							  -> nSamples_MH rows each containing a S-element row vector 
% trajDir_PROJ				- [str] path to directory holding i=1..S dim_TRAJ-dimensional trajectories in files traj_i.txt recording trajectory of dim_TRAJ-dimensional projection variables xi recorded simultaneously with umbrella variables over each biased simulation 
%							  -> each file trajDir_PROJ/traj_i.txt contains N_i rows constituting the number of samples recorded over the run -- which must be collected simultaneoulsy with umbrella variables in trajDir_UMB/traj_i.txt -- each containing dim_TRAJ columns recording the value of the projection variables xi(1:dim_TRAJ) 
% histBinEdgesFile_PROJ		- [str] path to text file containing dim_PROJ rows specifying edges of the rectilinear histogram bins in each dimension that will be used within this code to construct dim_PROJ-dimensional histograms and infer the reweighted unbiased free energy projection into the projection variables 
%							  -> histBinEdgesFile_PROJ contains k=1..dim_PROJ lines each holding a row vector specifying the rectilinear histogram bin edges in dimension k 
%                             -> for M_k_PROJ bins in dimension k, there are (M_k_PROJ+1) edges 
%                             -> bins need not be regularly spaced 

% OUTPUTS
%
% hist_binCenters_PROJ.txt	- [dim x M_k_PROJ float] text file containing dim_PROJ rows specifying M_k_PROJ k=1..dim_PROJ centers of the rectilinear histogram bins in each dimension used to construct dim_PROJ-dimensional histograms constituting pdf_PROJ_MAP.txt/pdf_PROJ_MH.txt and betaF_PROJ_MAP.txt/betaF_PROJ_MH.txt 
% hist_binWidths_PROJ.txt	- [dim x M_k_PROJ float] text file containing dim_PROJ rows specifying M_k_PROJ k=1..dim_PROJ widths of the rectilinear histogram bins in each dimension used to construct dim_PROJ-dimensional histograms constituting pdf_PROJ_MAP.txt/pdf_PROJ_MH.txt and betaF_PROJ_MAP.txt/betaF_PROJ_MH.txt 
% p_PROJ_MAP.txt			- [1 x M_PROJ float] text file containing MAP estimate of unbiased probability distribution p_l_PROJ_MAP over l=1..M_PROJ bins of dim_PROJ-dimensional rectilinear histogram 
%							  -> values recorded in row major order (last index changes fastest) 
% pdf_PROJ_MAP.txt			- [1 x M_PROJ float] text file containing MAP estimate of unbiased probability density function pdf_l_PROJ_MAP over l=1..M_PROJ bins of dim_PROJ-dimensional rectilinear histogram 
%							  -> values recorded in row major order (last index changes fastest) 
% betaF_PROJ_MAP.txt 		- [1 x M_PROJ float] text file containing MAP estimate of unbiased free energy surface betaF_l_PROJ_MAP = -ln(p(psi)/binVolume) + const. over l=1..M_PROJ bins of dim_PROJ-dimensional rectilinear histogram  
%							  -> values recorded in row major order (last index changes fastest) 
% p_PROJ_MH.txt				- [nSamples_MH x M_PROJ float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of unbiased probability distribution p_l_PROJ_MAP over l=1..M_PROJ bins of dim_PROJ-dimensional rectilinear histogram 
%							  -> values recorded in row major order (last index changes fastest) 
% pdf_PROJ_MH.txt			- [nSamples_MH x M_PROJ float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of unbiased probability density function pdf_l_PROJ_MAP over l=1..M_PROJ bins of dim_PROJ-dimensional rectilinear histogram 
%							  -> values recorded in row major order (last index changes fastest) 
% betaF_PROJ_MH.txt			- [nSamples_MH x M_PROJ float] text file containing nSamples_MH Metropolis-Hastings samples from the Bayes posterior of unbiased free energy surface betaF_l_PROJ_MAP = -ln(p(psi)/binVolume) + const. over l=1..M_PROJ bins of dim_PROJ-dimensional rectilinear histogram  
%							  -> values recorded in row major order (last index changes fastest) 


%% parameters
kB = 0.0083144621;      % Boltzmann's constant / kJ/mol.K
beta = 1/(kB*T);        % beta = 1/kBT = (kJ/mol)^(-1)
barTOkJpermolpernm3 = 0.06022140858;    % kJ/mol.nm^3 / bar


%% printing inputs to screen
fprintf('\n')
fprintf('T = %e\n',T)
fprintf('P = %e\n',P)
fprintf('dim_UMB = %d\n',dim_UMB)
fprintf('periodicity_UMB = [%s]\n',int2str(periodicity_UMB))
fprintf('TPFile_UMB = %s\n',TPFile_UMB)
fprintf('harmonicBiasesFile_UMB = %s\n',harmonicBiasesFile_UMB)
fprintf('trajDir_UMB = %s\n',trajDir_UMB)
fprintf('histBinEdgesFile_UMB = %s\n',histBinEdgesFile_UMB)
fprintf('histDir_UMB = %s\n',histDir_UMB)
fprintf('fMAPFile_UMB = %s\n',fMAPFile_UMB)
fprintf('fMHFile_UMB = %s\n',fMHFile_UMB)
fprintf('trajDir_PROJ = %s\n',trajDir_PROJ)
fprintf('histBinEdgesFile_PROJ = %s\n',histBinEdgesFile_PROJ)
fprintf('\n')


%% checking inputs
Dim_UMB = 2+dim_UMB;    % dimensionality of augmented histograms collected in [U,V,psi]; dim 1 = U, dim 2 = V, dim 3+ = psi 
periodicity_UMB = cat(2,[0 0],periodicity_UMB);


%% loading data

fprintf('Loading umbrella simulation data...\n')

% loading location and strength of harmonic biasing potentials in each of S umbrella simulations 
AA = importdata(harmonicBiasesFile_UMB,' ',0);
S = size(AA,1);                             % # umbrella simualtions
if size(AA,2) ~= (1+2*dim_UMB)
    error('Number of columns in %s ~= 1+2*dim_UMB = %d as expected',harmonicBiasesFile_UMB,1+2*dim_UMB);
end
umbC = AA(:,2:1+dim_UMB);           % centers of S dim_UMB-dimensional harmonic umbrella potentials / (arbitrary units)
umbF = AA(:,2+dim_UMB:1+2*dim_UMB); % force constants of S umbrella harmonic umbrella potentials / kJ/mol.(arbitrary units)^2 
clear AA


% loading temperature and pressure at which each of S umbrella simulations were conducted 
AA = importdata(TPFile_UMB,' ',0);
if size(AA,1) ~= S
    error('Number of rows in %s ~= %d as expected to be commensurate with %s',TPFile_UMB,S,harmonicBiasesFile);
end
if size(AA,2) ~= (1+2)
    error('Number of columns in %s ~= 3 as expected',TPFile_UMB);
end
umbT = AA(:,2);     % temperature at which each simulation was performed / K
umbP = AA(:,3);     % pressure at which each simulation was performed / bar 
clear AA


% loading umbrella histogram bin edges 
binE = cell(Dim_UMB,1);
fid = fopen(histBinEdgesFile_UMB,'rt');
for d = 1:Dim_UMB
    tline = fgetl(fid);
    if ~ischar(tline)
        error('%s contains fewer than expected number of Dim_UMB = %d lines specifying histogram bin edges',histBinEdgesFile_UMB,Dim_UMB);
    end
    binE{d} = sscanf(tline,'%f')';
end
tline = fgetl(fid);
if ischar(tline)
    error('%s contains more than expected number of Dim_UMB = %d lines specifying histogram bin edges',histBinEdgesFile_UMB,Dim_UMB);
end
fclose(fid);

% - counting number of bins in each dimension M_k k=1..Dim_UMB, and total number of bins M = product_k=1..Dim_UMB M_k = M_1*M_2*...*M_Dim_UMB 
M_k = nan(Dim_UMB,1);
for d = 1:Dim_UMB
    M_k(d) = length(binE{d})-1;
end
M = prod(M_k);

% - converting umbrella binEdges into binCenters and binWidths
binC = cell(Dim_UMB,1);
binW = cell(Dim_UMB,1);
for d = 1:Dim_UMB
    binC{d} = 0.5 * ( binE{d}(1:end-1) + binE{d}(2:end) );
    binW{d} = binE{d}(2:end) - binE{d}(1:end-1);
end

% - computing period in each periodic umbrella dimension as histogram bin range 
period = nan(1,Dim_UMB);
for d=1:Dim_UMB
    if periodicity_UMB(d) ~= 0
        period(d) = binE{d}(end)-binE{d}(1);
    end
end


% loading histograms compiled over the S umbrella simulations recorded as row vectors in row major order (last index changes fastest) 
n_il = nan(S,M);
for i = 1:S
    AA = importdata([histDir_UMB,'/hist_',int2str(i),'.txt'],' ',0);
    if size(AA,1) ~= 1
        error('Did not find expected row vector in reading %s',[histDir_UMB,'/hist_',int2str(i),'.txt']);
    end
    if size(AA,2) ~= M
        error('Row vector in %s did not contain expected number of elements M = M_1*M_2*...*M_Dim_UMB = %d given histogram bins specified in %s',[histDir_UMB,'/hist_',int2str(i),'.txt'],M,histBinEdgesFile_UMB);
    end
    n_il(i,:) = AA;
    clear AA
    fprintf('\tLoading of biased simulation histogram %d of %d complete\n',i,S)
end


% precomputing umbrella aggregated statistics
N_i = sum(n_il,2);          % total counts in simulation i
M_l = sum(n_il,1)';         % total counts in bin l


fprintf('DONE!\n\n')


fprintf('Loading MAP and MH f values simulation data...\n')

AA = importdata(fMAPFile_UMB);
if size(AA,1) ~= 1
    error('Did not find expected row vector in reading %s',fMAPFile_UMB);
end
if size(AA,2) ~= S
    error('Row vector in %s did not contain expected number of elements %d corresponding to number of simulations in directory %s',fMAPFile_UMB,S,histDir_UMB);
end
f_i_MAP = AA;
clear AA

AA = importdata(fMHFile_UMB);
nSamples_MH = size(AA,1);
if size(AA,2) ~= S
    error('Number of columns in %s did not contain expected number of elements %d corresponding to number of simulations in directory %s',fMHFile_UMB,S,histDir_UMB);
end
f_i_MH = AA;
clear AA

fprintf('DONE!\n\n')


fprintf('Loading umbrella trajectory...\n')

traj = cell(S,1);
for i = 1:S
    AA = importdata([trajDir_UMB,'/traj_',int2str(i),'.txt']);
    if size(AA,2) ~= Dim_UMB
        error('Dimensionality of %s is not Dim_UMB = (2+dimUMB) = %d as expected',[trajDir_UMB,'/traj_',int2str(i),'.txt'],Dim_UMB);
    end
    if size(AA,1) ~= N_i(i)
        error('Number of samples in %s does not match number of counts in histogram loaded from %s',[trajDir_UMB,'/traj_',int2str(i),'.txt'],[histDir_UMB,'/hist_',int2str(i),'.txt']);
    end
    traj{i} = AA;
    clear AA
end

fprintf('DONE!\n\n')


fprintf('Loading projection trajectory...\n')

traj_PROJ = cell(S,1);
for i = 1:S
    AA = importdata([trajDir_PROJ,'/traj_',int2str(i),'.txt']);
    if i==1
        dim_PROJ = size(AA,2);
    else
        if size(AA,2) ~= dim_PROJ
            error('Dimensionality of %s does not match that of %s',[trajDir_PROJ,'/traj_',int2str(i),'.txt'],[trajDir_PROJ,'/traj_1.txt']);
        end
    end
    if size(AA,1) ~= N_i(i)
        error('Number of samples in %s does not match number of counts in histogram loaded from %s',[trajDir_PROJ,'/traj_',int2str(i),'.txt'],[histDir_UMB,'/hist_',int2str(i),'.txt']);
    end
    traj_PROJ{i} = AA;
    clear AA
end

fprintf('DONE!\n\n')


fprintf('Loading projection histogram bin edges...\n')

binE_PROJ = cell(dim_PROJ,1);
fid = fopen(histBinEdgesFile_PROJ,'rt');
for d = 1:dim_PROJ
    tline = fgetl(fid);
    if ~ischar(tline)
        error('%s contains fewer than expected number of dim_PROJ = %d lines specifying histogram bin edges',histBinEdgesFile_PROJ,dim_PROJ);
    end
    binE_PROJ{d} = sscanf(tline,'%f')';
end
tline = fgetl(fid);
if ischar(tline)
    error('%s contains more than expected number of dim_PROJ = %d lines specifying histogram bin edges',histBinEdgesFile_PROJ,dim_PROJ);
end
fclose(fid);

% - counting number of bins in each dimension M_k_PROJ k=1..dim_PROJ, and total number of bins M_PROJ = product_k=1..dim_PROJ M_k_PROJ = M_PROJ_1*M_PROJ_2*...*M_PROJ_dim_PROJ 
M_k_PROJ = nan(dim_PROJ,1);
for d = 1:dim_PROJ
    M_k_PROJ(d) = length(binE_PROJ{d})-1;
end
M_PROJ = prod(M_k_PROJ);

% - converting umbrella binEdges into binCenters and binWidths
binC_PROJ = cell(dim_PROJ,1);
binW_PROJ = cell(dim_PROJ,1);
for d = 1:dim_PROJ
    binC_PROJ{d} = 0.5 * ( binE_PROJ{d}(1:end-1) + binE_PROJ{d}(2:end) );
    binW_PROJ{d} = binE_PROJ{d}(2:end) - binE_PROJ{d}(1:end-1);
end

fprintf('DONE!\n\n')


%% precomputing biasing potentials for each simulation i in each bin l 
fprintf('Computing biasing potentials for each umbrella simulation in each bin...\n')
c_il = nan(S,M);            % S-by-M matrix containing biases due to artificial harmonic potential for simulation i in bin l
for i=1:S
    for l=1:M
        
        sub = ind2sub_RMO(M_k',l);
        
        expArg=0;
        
        d=1;
        expArg = expArg + ( 1/(kB*umbT(i)) - beta )*binC{d}(sub(d));
        
        d=2;
        expArg = expArg + ( umbP(i)*barTOkJpermolpernm3/(kB*umbT(i)) - P*barTOkJpermolpernm3*beta )*binC{d}(sub(d));
        
        for d=3:Dim_UMB
            if periodicity_UMB(d) ~= 0
                delta = min( [ abs(binC{d}(sub(d))-umbC(i,d-2)), abs(binC{d}(sub(d))-umbC(i,d-2)+period(d)), abs(binC{d}(sub(d))-umbC(i,d-2)-period(d)) ] );
            else
                delta = abs(binC{d}(sub(d))-umbC(i,d-2));
            end
            expArg = expArg + 1/(kB*umbT(i))*0.5*umbF(i,d-2)*delta^2;
        end
        
        c_il(i,l) = exp(-expArg);
        
    end
    fprintf('\tProcessing of simulation %d of %d complete\n',i,S)
end
fprintf('DONE!\n\n')


%% performing MAP and MH projections into projection trajectory variables

fprintf('Performing MAP and MH projection...\n')

p_l_PROJ_MAP = zeros(M_PROJ,1);
p_l_PROJ_MH = zeros(M_PROJ,nSamples_MH);

for i=1:S
    for k=1:N_i(i)
        
        % traj_PROJ
        
        % - extracting
        traj_PROJ_ik = traj_PROJ{i}(k,:);
        
        % - bound checking
        for d=1:dim_PROJ
            if ( ( traj_PROJ_ik(d) < binE_PROJ{d}(1) ) || ( traj_PROJ_ik(d) >= binE_PROJ{d}(end) ) )
                fprintf('\tWARNING -- dimension %d of entry %d in projection trajectory file %s = %e is out of projection histogram bounds [%e,%e); this observation will be skipped\n',d,k,[trajDir_PROJ,'/traj_',int2str(i),'.txt'],traj_PROJ_ik(d),binE_PROJ{d}(1),binE_PROJ{d}(end));
            end
        end
        
        % - indexing
        sub_PROJ = binner(traj_PROJ_ik,binE_PROJ);
        if sub_PROJ(1) < 0
            continue;
        end
        idx_PROJ = sub2ind_RMO(M_k_PROJ',sub_PROJ);
        
        
        % traj
        
        % - extracting
        traj_ik = traj{i}(k,:);
        
        % - bound checking
        for d=1:Dim_UMB
            if ( ( traj_ik(d) == binE{d}(end) ) && ( periodicity_UMB(d) ~= 0 ) )
                traj_ik(d) = traj_ik(d) - period(d);
            end
            if ( ( traj_ik(d) < binE{d}(1) ) || ( traj_ik(d) >= binE{d}(end) ) )
                fprintf('\tWARNING -- dimension %d of entry %d in umbrella trajectory file %s = %e is out of umbrella histogram bounds [%e,%e); this observation will be skipped\n',d,k,[trajDir_UMB,'/traj_',int2str(i),'.txt'],traj_ik(d),binE{d}(1),binE{d}(end));
            end
        end
        
        % - indexing
        sub_UMB = binner(traj_ik,binE);
        if sub_UMB(1) < 0
            continue;
        end
        idx_UMB = sub2ind_RMO(M_k',sub_UMB);
        
        
        % accumulating reweighting
        p_l_PROJ_MAP(idx_PROJ) = p_l_PROJ_MAP(idx_PROJ) + 1./sum(N_i.*c_il(:,idx_UMB).*f_i_MAP');
        for q=1:nSamples_MH
            p_l_PROJ_MH(idx_PROJ,q) = p_l_PROJ_MH(idx_PROJ,q) + 1./sum(N_i.*c_il(:,idx_UMB).*f_i_MH(q,:)');
        end
        
    end
    fprintf('\tProjection of simulation %d of %d complete\n',i,S)
end

fprintf('DONE!\n\n')


% - normalizing p_l_PROJ_MAP and p_l_PROJ_MH
p_l_PROJ_MAP = p_l_PROJ_MAP ./ sum(p_l_PROJ_MAP);
for q=1:nSamples_MH
    p_l_PROJ_MH(:,q) = p_l_PROJ_MH(:,q) ./ sum(p_l_PROJ_MH(:,q));
end


% - converting p_l_PROJ_MAP into probability density function pdf_l_PROJ_MAP and free energy estimate betaF_l_PROJ_MAP 
%   -> mean zeroing betaF; when plotting multiple free energy surfaces this is equivalent to minimizing the mean squared error between the landscapes 
pdf_l_PROJ_MAP = zeros(size(p_l_PROJ_MAP));
betaF_l_PROJ_MAP = nan(size(p_l_PROJ_MAP));
for l=1:M_PROJ
    sub_PROJ = ind2sub_RMO(M_k_PROJ',l);
    binVol_PROJ=1;
    for d=1:dim_PROJ
        binVol_PROJ = binVol_PROJ*binW_PROJ{d}(sub_PROJ(d));
    end
    if p_l_PROJ_MAP(l) > 0
        pdf_l_PROJ_MAP(l) = p_l_PROJ_MAP(l)/binVol_PROJ;
        betaF_l_PROJ_MAP(l) = -log(p_l_PROJ_MAP(l)/binVol_PROJ);
    end
end
betaF_l_PROJ_MAP = betaF_l_PROJ_MAP - mean(betaF_l_PROJ_MAP(isfinite(betaF_l_PROJ_MAP)));

% - writing to file bin centers and bin widths of l=1..M_PROJ bins of dim_PROJ-dimensional rectilinear projection histogram grid as row vectors in row major order (last index changes fastest) 
fout = fopen('hist_binCenters_PROJ.txt','wt');
    for d=1:dim_PROJ
        fprintf(fout,'%15.5e',binC_PROJ{d});
        fprintf(fout,'\n');
    end
fclose(fout);

fout = fopen('hist_binWidths_PROJ.txt','wt');
    for d=1:dim_PROJ
        fprintf(fout,'%15.5e',binW_PROJ{d});
        fprintf(fout,'\n');
    end
fclose(fout);

% - writing to file p_l_PROJ_MAP, pdf_l_PROJ_MAP, and betaF_l_PROJ_MAP over l=1..M_PROJ bins of dim_PROJ-dimensional rectilinear projection histogram grid as row vectors in row major order (last index changes fastest) 
fout = fopen('p_PROJ_MAP.txt','wt');
    fprintf(fout,'%15.5e',p_l_PROJ_MAP);
    fprintf(fout,'\n');
fclose(fout);

fout = fopen('pdf_PROJ_MAP.txt','wt');
    fprintf(fout,'%15.5e',pdf_l_PROJ_MAP);
    fprintf(fout,'\n');
fclose(fout);

fout = fopen('betaF_PROJ_MAP.txt','wt');
    fprintf(fout,'%15.5e',betaF_l_PROJ_MAP);
    fprintf(fout,'\n');
fclose(fout);


% - converting p_l_PROJ_MH into pdf_l_PROJ_MH and betaF_l_PROJ_MH
%   -> mean zeroing betaF; when plotting multiple free energy surfaces this is equivalent to minimizing the mean squared error between the landscapes 
pdf_l_PROJ_MH = zeros(size(p_l_PROJ_MH));
betaF_l_PROJ_MH = nan(size(p_l_PROJ_MH));
for l=1:M_PROJ
    sub_PROJ = ind2sub_RMO(M_k_PROJ',l);
    binVol_PROJ=1;
    for d=1:dim_PROJ
        binVol_PROJ = binVol_PROJ*binW_PROJ{d}(sub_PROJ(d));
    end
    for jj=1:size(p_l_PROJ_MH,2)
        if p_l_PROJ_MH(l,jj) > 0
            pdf_l_PROJ_MH(l,jj) = p_l_PROJ_MH(l,jj)/binVol_PROJ;
            betaF_l_PROJ_MH(l,jj) = -log(p_l_PROJ_MH(l,jj)/binVol_PROJ);
        end
    end
end
betaF_l_PROJ_MH_colMean = nan(1,nSamples_MH);
for jj=1:nSamples_MH
    betaF_l_PROJ_MH_col = betaF_l_PROJ_MH(:,jj);
    betaF_l_PROJ_MH_colMean(jj) = mean(betaF_l_PROJ_MH_col(isfinite(betaF_l_PROJ_MH_col)));
end
betaF_l_PROJ_MH = betaF_l_PROJ_MH - repmat( betaF_l_PROJ_MH_colMean, size(betaF_l_PROJ_MH,1), 1);

% - writing to file p_l_PROJ_MH, pdf_l_PROJ_MH, and betaF_l_PROJ_MH over l=1..M_PROJ bins of dim_PROJ-dimensional rectilinear projection histogram grid as row vectors in row major order (last index changes fastest) 
fout = fopen('p_PROJ_MH.txt','wt');
    for jj=1:nSamples_MH
        fprintf(fout,'%15.5e',p_l_PROJ_MH(:,jj));
        fprintf(fout,'\n');
    end
fclose(fout);

fout = fopen('pdf_PROJ_MH.txt','wt');
    for jj=1:nSamples_MH
        fprintf(fout,'%15.5e',pdf_l_PROJ_MH(:,jj));
        fprintf(fout,'\n');
    end
fclose(fout);

fout = fopen('betaF_PROJ_MH.txt','wt');
    for jj=1:nSamples_MH
        fprintf(fout,'%15.5e',betaF_l_PROJ_MH(:,jj));
        fprintf(fout,'\n');
    end
fclose(fout);







end











%---%
function sub = ind2sub_RMO(sz,ind)

% function to convert element index in a matrix of size sz to a subscripted location sub under row major ordering (RMO) 

% error checking
if size(sz,1) ~= 1
    error('Variable sz in ind2sub_RMO must be a row vector');
end
dim = size(sz,2);

ind_max = prod(sz);
if ( (ind < 1) || (ind > ind_max) )
    error('Variable ind = %d < 1 or ind = %d > # elements in tensor of size sz = %d in ind2sub_RMO',ind,ind,prod(sz));
end

% converting ind to sub
sub = nan(1,dim);
for ii=1:(dim-1)
    P=prod(sz(ii+1:end));
    sub_ii = ceil(ind/P);
    sub(ii) = sub_ii;
    ind = ind - (sub_ii-1)*P;
end
sub(end) = ind;

end

%---%
function ind = sub2ind_RMO(sz,sub)

% function to convert subscripted location sub in a matrix of size sz to element index under row major ordering (RMO) 

% error checking
if size(sz,1) ~= 1
    error('Variable sz in sub2ind_RMO must be a row vector');
end
dim = size(sz,2);

if size(sz,1) ~= 1
    error('Variable sub in sub2ind_RMO must be a row vector');
end

if size(sz,2) ~= size(sub,2)
    error('Variables sub and sz in sub2ind_RMO are not commensurate');
end

for ii=1:dim
    if ( sub(ii) < 1 || sub(ii) > sz(ii) )
        error('sub(%d) = %d < 1 or sub(%d) = %d > sz(%d) = %d in sub2ind_RMO',ii,sub(ii),ii,sub(ii),ii,sz(ii));
    end
end

% converting sub to ind
ind=0;
for ii=1:(dim-1)
    ind = ind + (sub(ii)-1)*prod(sz(ii+1:end));
end
ind = ind + sub(end);

end

%---%
function sub = binner(valVec,binE)

% function to place a dim-dimensional vector valVec into a dim-dimensional histogram with bin edges defined by binE 

% error checking
if size(valVec,1) ~= 1
    error('valVec = %f supplied to function binner is not a row vector',valVec);
end

dim = size(valVec,2);
if length(binE) ~= dim
    error('Dimensionality %d of row vector valVec ~= dimensionality %d of binE',size(valVec,2),length(binE));
end

% binning values in valVec
sub = nan(1,dim);
for d=1:dim
    if ( ( valVec(d) < binE{d}(1) ) || ( valVec(d) >= binE{d}(end) ) )
        sub = -ones(1,dim);     % if at least one value of valVec falls outside histogram bins defined by binE returning -1's vector
        return;
    end
    for i=1:length(binE{d})
        if ( ( valVec(d) >= binE{d}(i) ) && ( valVec(d) < binE{d}(i+1) ) )
            sub(d) = i;
            break;
        end
    end
end

end



