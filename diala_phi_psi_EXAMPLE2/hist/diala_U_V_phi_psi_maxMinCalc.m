function diala_U_V_phi_psi_maxMinCalc

U_min=+Inf;
U_max=-Inf;
V_min=+Inf;
V_max=-Inf;
phi_min=+Inf;
phi_max=-Inf;
psi_min=+Inf;
psi_max=-Inf;

M=36;
for i=1:M
	
    % loading trajectory data
    fin = fopen(['../traj/traj_',int2str(i),'.txt'],'rt');
	tmp = textscan(fin,'%f%f%f%f','headerlines',0);
    U_V_phi_psi = [tmp{1},tmp{2},tmp{3},tmp{4}];
	fclose(fin);
    
    U_min = min(U_min,min(U_V_phi_psi(:,1)));
    U_max = max(U_max,max(U_V_phi_psi(:,1)));
    V_min = min(V_min,min(U_V_phi_psi(:,2)));
    V_max = max(V_max,max(U_V_phi_psi(:,2)));
    phi_min = min(phi_min,min(U_V_phi_psi(:,3)));
    phi_max = max(phi_max,max(U_V_phi_psi(:,3)));
    psi_min = min(psi_min,min(U_V_phi_psi(:,4)));
    psi_max = max(psi_max,max(U_V_phi_psi(:,4)));
    
end

fprintf('[U_min, U_max] = [%.5f, %.5f]\n',U_min,U_max);
fprintf('[V_min, V_max] = [%.5f, %.5f]\n',V_min,V_max);
fprintf('[phi_min, phi_max] = [%.5f, %.5f]\n',phi_min,phi_max);
fprintf('[psi_min, psi_max] = [%.5f, %.5f]\n',psi_min,psi_max);
