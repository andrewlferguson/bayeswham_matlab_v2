All instances of " 180.000" converted to "-180.000" in text files in all ./traj* directories using:

perl -p -i -e 's/ 180.000/-180.000/g' *.txt

Checked for success by running:

grep " 180.000" ./*